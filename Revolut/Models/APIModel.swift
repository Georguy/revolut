//
//  APIModel.swift
//  Revolut
//
//  Created by Georgij Emelyanov on 26/01/2019.
//  Copyright © 2019 Georgij Emelyanov. All rights reserved.
//

import Foundation

protocol APIModel { }
typealias APIModelCodable = APIModel & Codable

extension APIModel where Self: Codable {
    static func from(data: Data) throws -> Self  {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        return try decoder.decode(Self.self, from: data)
    }
}
