//
//  CurrenciesResponse.swift
//  Revolut
//
//  Created by Georgij Emelyanov on 26/01/2019.
//  Copyright © 2019 Georgij Emelyanov. All rights reserved.
//

import Foundation

typealias ISO = String

struct CurrenciesResponse: APIModelCodable {
    let base: ISO
    let date: String
    let rates: [Currency]
    
    private let dictRates: [ISO: Double]
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        base = try! container.decode(ISO.self, forKey: .base)
        date = try! container.decode(String.self, forKey: .date)
        dictRates = try! container.decode([ISO: Double].self, forKey: .dictRates)
        
        rates = dictRates.map { Currency(iso: $0.key, rate: $0.value) }
    }
    
    enum CodingKeys: String, CodingKey {
        case base
        case date
        case dictRates = "rates"
    }
}

extension CurrenciesResponse {
    init(base: String = "EUR", date: String = "", rates: [Currency] = []) {
        self.base = base
        self.date = date
        self.rates = rates
        self.dictRates = ["" : 0.0]
    }
}
