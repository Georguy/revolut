//
//  Currency.swift
//  Revolut
//
//  Created by Georgij Emelyanov on 26/01/2019.
//  Copyright © 2019 Georgij Emelyanov. All rights reserved.
//

import Foundation

struct Currency: Hashable {
    let iso: ISO
    let rate: Double
    
    static func == (lhs: Currency, rhs: Currency) -> Bool {
        return lhs.iso == rhs.iso
    }
}

struct CountedCurrency {
    let iso: ISO
    let valueString: String
}
