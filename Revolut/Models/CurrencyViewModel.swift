//
//  CurrencyViewModel.swift
//  Revolut
//
//  Created by Georgij Emelyanov on 04/02/2019.
//  Copyright © 2019 Georgij Emelyanov. All rights reserved.
//

import Foundation

struct CurrencyViewModel: Hashable {
    let iso: ISO
    let sumString: String
    
    static func == (lhs: CurrencyViewModel, rhs: CurrencyViewModel) -> Bool {
        return lhs.iso == rhs.iso
    }
}
