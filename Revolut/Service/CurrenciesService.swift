//
//  CurrenciesServer.swift
//  Revolut
//
//  Created by Georgij Emelyanov on 26/01/2019.
//  Copyright © 2019 Georgij Emelyanov. All rights reserved.
//

import Foundation

protocol CurrenciesServiceProtocol {
    func fetchCurrencies(by base: ISO, completion: @escaping ((Result<CurrenciesResponse>) -> Void))
}

class CurrenciesService: CurrenciesServiceProtocol {
    private let networkManager: NetworkManagerProtocol
    init(networkManager: NetworkManagerProtocol = NetworkManager()) {
        self.networkManager = networkManager
    }
    
    func fetchCurrencies(by base: ISO, completion: @escaping ((Result<CurrenciesResponse>) -> Void)) {
        networkManager.getData(by: base) { result in
            switch result {
            case let .success(value):
                do {
                    let result = try CurrenciesResponse.from(data: value)
                    completion(.success(result))
                } catch {
                    completion(.failure(error))
                }
                
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
}
