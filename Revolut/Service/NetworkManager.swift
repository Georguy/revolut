//
//  NetworkManager.swift
//  Revolut
//
//  Created by Georgij Emelyanov on 26/01/2019.
//  Copyright © 2019 Georgij Emelyanov. All rights reserved.
//

import Foundation

enum Result<Value> {
    case success(Value)
    case failure(Error)
}

protocol NetworkManagerProtocol {
    func getData(by base: ISO, completion: @escaping (Result<Data>) -> Void)
}

class NetworkManager: NetworkManagerProtocol {
    func getData(by base: ISO, completion: @escaping (Result<Data>) -> Void) {
        let url = URL(string: "https://revolut.duckdns.org/latest?base=\(base)")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                guard responseError == nil else {
                    completion(.failure(responseError!))
                    return
                }
                
                guard let data = responseData else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Data was not retrieved from request"]) as Error
                    completion(.failure(error))
                    return
                }
                
                completion(.success(data))
            }
        }
        task.resume()
    }
}
