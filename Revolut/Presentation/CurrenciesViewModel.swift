//
//  CurrenciesViewModel.swift
//  Revolut
//
//  Created by Georgij Emelyanov on 26/01/2019.
//  Copyright © 2019 Georgij Emelyanov. All rights reserved.
//

import Foundation

protocol CurrenciesViewModelProtocol {
    var onChange: (([CurrencyViewModel], Bool) -> Void)? { get set }
    var onError: (() -> ())? { get set }
    
    func getCurrencies(by base: ISO)
    func updateValueString(_ valueString: String)
    func updateBaseCurrency(_ currencyViewModel: CurrencyViewModel, atIndex index: Int)
}

class CurrenciesViewModel: CurrenciesViewModelProtocol {
    private let currenciesService: CurrenciesServiceProtocol
    private let currenciesWorker: CurrenciesResponseWorkerProtocol
    private let currenciesConverter: CurrenciesConverterProtocol
    
    private var value: Double = 1
    private var base: ISO = "EUR"
    private var currencies: [Currency] = .init()
    
    private var isForceReload = false
    
    var onChange: (([CurrencyViewModel], Bool) -> Void)?
    var onError: (() -> ())?
    
    private weak var timer: Timer?
    
    init(currenciesService: CurrenciesServiceProtocol = CurrenciesService(),
         currenciesWorker: CurrenciesResponseWorkerProtocol = CurrenciesResponseWorker(),
         currenciesConverter: CurrenciesConverterProtocol = CurrenciesConverter()) {
        self.currenciesService = currenciesService
        self.currenciesWorker = currenciesWorker
        self.currenciesConverter = currenciesConverter
        
        createTimer()
    }
    
    func getCurrencies(by base: ISO) {
        self.base = base
        
        currenciesService.fetchCurrencies(by: base) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success(response):
                guard response.base == self.base else { return }
                
                self.currenciesWorker.handleResponse(response, oldCurrencies: self.currencies) { newCurrencies in
                    self.currencies = newCurrencies
                    self.currenciesConverter.convert(newCurrencies, value: self.value, completion: { self.onChange?($0, self.isForceReload) })
                    self.isForceReload = false
                }
                
            case .failure:
                self.onError?()
            }
        }
    }
    
    func updateValueString(_ valueString: String) {
        value = currenciesConverter.convertStringToDouble(valueString) ?? 1
        currenciesConverter.convert(currencies, value: value, completion: { self.onChange?($0, self.isForceReload) })
    }
    
    func updateBaseCurrency(_ currencyViewModel: CurrencyViewModel, atIndex index: Int) {
        value = currenciesConverter.convertStringToDouble(currencyViewModel.sumString) ?? 1
        currencies.move(from: index, to: 0)
        base = currencyViewModel.iso
        isForceReload = true
        getCurrencies(by: base)
    }
    
    @objc func loadCurrenciesByTimer() {
        getCurrencies(by: base)
    }
    
    deinit {
        invlidateTimer()
    }
    
    private func createTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(loadCurrenciesByTimer), userInfo: nil, repeats: true)
    }
    private func invlidateTimer() {
        timer?.invalidate()
    }
}
