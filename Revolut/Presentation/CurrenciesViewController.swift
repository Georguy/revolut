//
//  CurrenciesViewController.swift
//  Revolut
//
//  Created by Georgij Emelyanov on 26/01/2019.
//  Copyright © 2019 Georgij Emelyanov. All rights reserved.
//

import UIKit

class CurrenciesViewController: UIViewController {
    @IBOutlet private var tableView: UITableView!
    private var viewModel: CurrenciesViewModelProtocol!
    private var currencies: [CurrencyViewModel] = .init()
    private lazy var errorBanner = ErrorBanner()
    private var canReload = true
    
    override func loadView() {
        super.loadView()
        view.addSubview(errorBanner)
        errorBanner.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        errorBanner.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10).isActive = true
        errorBanner.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -60).isActive = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        configureViewModel()
    }
    
    private func registerCells() {
        tableView.register(UINib(nibName: String(describing: CurrencyTableViewCell.self), bundle: nil),
                           forCellReuseIdentifier: String(describing: CurrencyTableViewCell.self))
    }
    
    private func configureViewModel() {
        viewModel = CurrenciesViewModel()
        viewModel.onChange = { cVM, isForceReload in
            self.handleNewCurrencies(cVM, isForceReload: isForceReload)
            self.errorBanner.hide()
        }
        viewModel.onError = {
            self.errorBanner.show()
        }
        viewModel.getCurrencies(by: "EUR")
    }
}

extension CurrenciesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currencies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CurrencyTableViewCell.self),
                                                       for: indexPath) as? CurrencyTableViewCell else { return UITableViewCell() }
        
        configure(cell: cell, with: currencies[indexPath.row], isEnabled: indexPath.row == 0)
        cell.delegate = self
        return cell
    }
}

extension CurrenciesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard indexPath.row != 0 else { becomeFirstResponderIfNeeded(); return }
        moveCellToTop(in: tableView, atIndexPath: indexPath)
    }
}

extension CurrenciesViewController: CurrencyTableViewCellDelegate {
    func currencyTableViewCell(_ cell: CurrencyTableViewCell, didChangeValue valueString: String) {
        viewModel.updateValueString(valueString)
    }
}

extension CurrenciesViewController {
    private func handleNewCurrencies(_ currenciesViewModel: [CurrencyViewModel], isForceReload: Bool) {
        guard self.canReload || isForceReload else { return }
        
        if !self.currencies.isEmpty {
            self.currencies = currenciesViewModel
            self.updateVisibleCells()
        } else {
            self.currencies = currenciesViewModel
            self.tableView.reloadData()
        }
                
        self.canReload = true
    }
    
    private func updateVisibleCells() {
        tableView.indexPathsForVisibleRows?.forEach {
            guard $0.row != 0 else { return }
            
            self.configure(cell: self.tableView.cellForRow(at: $0),
                           with: self.currencies[$0.row],
                           isEnabled: false)
        }
    }
    
    private func configure(cell: UITableViewCell?, with viewModel: CurrencyViewModel, isEnabled: Bool) {
        guard let cell = cell as? CurrencyTableViewCell else { return }
        
        cell.isoLabel.text = viewModel.iso
        cell.rateTextField.text = viewModel.sumString
        cell.rateTextField.isEnabled = isEnabled
        cell.icimageView.image = UIImage(named: viewModel.iso)
    }
    
    private func moveCellToTop(in tableView: UITableView, atIndexPath indexPath: IndexPath) {
        currencies.move(from: indexPath.row, to: 0)
        canReload = false
        
        CATransaction.setCompletionBlock { [weak self] in
            guard let self = self else { return }
            self.viewModel.updateBaseCurrency(self.currencies[0], atIndex: indexPath.row)
            self.becomeFirstResponderIfNeeded()
        }
        
        CATransaction.begin()
        
        let cellRect = tableView.rectForRow(at: IndexPath.firstRow)
        if tableView.bounds.contains(cellRect) {
            tableView.moveRow(at: indexPath, to: IndexPath.firstRow)
        } else {
            UIView.performWithoutAnimation {
                tableView.moveRow(at: indexPath, to: IndexPath.firstRow)
            }
        }
        
        tableView.scrollToRow(at: IndexPath.firstRow, at: .top, animated: true)
        CATransaction.commit()
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y == 0 {
            becomeFirstResponderIfNeeded()
        }
    }
    
    private func becomeFirstResponderIfNeeded() {
        let cell = tableView.cellForRow(at: IndexPath.firstRow) as? CurrencyTableViewCell
        cell?.rateTextField.isEnabled = true
        cell?.rateTextField.becomeFirstResponder()
    }
}
