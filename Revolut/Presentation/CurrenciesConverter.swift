//
//  CurrenciesConverter.swift
//  Revolut
//
//  Created by Georgij Emelyanov on 28/01/2019.
//  Copyright © 2019 Georgij Emelyanov. All rights reserved.
//

import Foundation

protocol CurrenciesConverterProtocol {
    func convert(_ currencies: [Currency], value: Double, completion: @escaping([CurrencyViewModel]) -> Void)
    func convertStringToDouble(_ sumString: String) -> Double?
}

class CurrenciesConverter: CurrenciesConverterProtocol {
    
    private let numberFormatter = NumberFormatter.numberFormatter()
    
    func convert(_ currencies: [Currency], value: Double, completion: @escaping([CurrencyViewModel]) -> Void) {
        let countedCurrencies: [CurrencyViewModel] = currencies.compactMap {
            guard let countedValueString = numberFormatter.stringFrom(Decimal($0.rate * value)) else { return nil }
            return CurrencyViewModel(iso: $0.iso, sumString: countedValueString)
        }
        
        completion(countedCurrencies)
    }
    
    func convertStringToDouble(_ sumString: String) -> Double? {
        var formattedString = sumString.replacingOccurrences(of: ".", with: numberFormatter.decimalSeparator)
        formattedString = formattedString.replacingOccurrences(of: ",", with: numberFormatter.decimalSeparator)
        
        return numberFormatter.number(from: formattedString)?.decimalValue.doubleValue
    }
    
}
