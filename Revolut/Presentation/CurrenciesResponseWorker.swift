//
//  CurrenciesResponseWorker.swift
//  Revolut
//
//  Created by Georgij Emelyanov on 26/01/2019.
//  Copyright © 2019 Georgij Emelyanov. All rights reserved.
//

import Foundation

protocol CurrenciesResponseWorkerProtocol {
    func handleResponse(_ response: CurrenciesResponse, oldCurrencies: [Currency], completion: @escaping([Currency]) -> Void)
}

class CurrenciesResponseWorker: CurrenciesResponseWorkerProtocol {
    func handleResponse(_ response: CurrenciesResponse, oldCurrencies: [Currency], completion: @escaping([Currency]) -> Void) {
        let newBaseCurrency = Currency(iso: response.base, rate: 1.0)
        
        var newCurrencies: [Currency] = [newBaseCurrency]
        
        if oldCurrencies.isEmpty {
            newCurrencies.append(contentsOf: response.rates)
            completion(newCurrencies)
            
        } else {
            DispatchQueue.global().async {
                var tempOldCurrencies = oldCurrencies
                tempOldCurrencies.remove(newBaseCurrency)
                
                let ratesSet = Set(response.rates)
                
                tempOldCurrencies.forEach { oldCurrency in
                    guard let newCurrency = ratesSet.first(where: { $0 == oldCurrency }) else { return }
                    newCurrencies.append(newCurrency)
                }
                DispatchQueue.main.async {
                    completion( newCurrencies )
                }
            }
        }
    }
}
