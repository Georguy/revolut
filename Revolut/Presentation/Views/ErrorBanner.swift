//
//  ErrorBanner.swift
//  Revolut
//
//  Created by Georgij Emelyanov on 06/02/2019.
//  Copyright © 2019 Georgij Emelyanov. All rights reserved.
//

import UIKit

class ErrorBanner: UIView {
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Reconnecting"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        return label
    }()
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(style: .white)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layer.cornerRadius = 5
        backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        alpha = 0
        
        addSubviews()
        configureConstraints()
    }
    
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(titleLabel)
        addSubview(activityIndicator)
    }
    
    private func configureConstraints() {
        translatesAutoresizingMaskIntoConstraints = false
        heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        activityIndicator.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
    
    func hide() {
        activityIndicator.stopAnimating()
        UIView.animate(withDuration: 0.3) {
            self.alpha = 0
        }
    }
    
    func show() {
        activityIndicator.startAnimating()
        UIView.animate(withDuration: 0.3) {
            self.alpha = 1
        }
    }
}
