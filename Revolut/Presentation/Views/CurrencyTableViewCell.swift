//
//  CurrencyTableViewCell.swift
//  Revolut
//
//  Created by Georgij Emelyanov on 26/01/2019.
//  Copyright © 2019 Georgij Emelyanov. All rights reserved.
//

import UIKit

protocol CurrencyTableViewCellDelegate: class {
    func currencyTableViewCell(_ cell: CurrencyTableViewCell, didChangeValue valueString: String)
}

class CurrencyTableViewCell: UITableViewCell {
    weak var delegate: CurrencyTableViewCellDelegate!
    
    @IBOutlet var icimageView: UIImageView!
    @IBOutlet var isoLabel: UILabel!
    @IBOutlet var rateTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        rateTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        rateTextField.delegate = self
    }

    @objc func textFieldDidChange(_ textField: UITextField) {
        if let value = textField.text, value.count > 0 {
            self.delegate.currencyTableViewCell(self, didChangeValue: value)
        } else {
            self.delegate.currencyTableViewCell(self, didChangeValue: "0")
        }
    }
}

extension CurrencyTableViewCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, text.isEmpty && (string == "." || string == ",") {
            textField.text = "0"
            return true
        }
        
        if let text = textField.text, text == "0" && string == "0" {
            return false
        }
        
        if let text = textField.text, text == "0" && (string != "0" && (string != "." || string != ",")) {
            textField.text = ""
            return true
        }
        
        guard !string.isEmpty else { return true }
        guard let text = textField.text, text.count < 9 else { return false }

        
        
        if let indexOfDot = text.index(of: Character("."))  {
            guard string != "." else { return false }
            guard text[indexOfDot.encodedOffset+1..<text.count].count < 2 else { return false }
            
            
            
        } else if let indexOfComma = text.index(of: Character(",")) {
            guard string != "," else { return false }
            guard text[indexOfComma.encodedOffset+1..<text.count].count < 2 else { return false }
            
            
        } else {
            return true
        }
        
        return true
    }
}
