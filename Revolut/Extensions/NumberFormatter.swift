//
//  NumberFormatter.swift
//  Revolut
//
//  Created by Georgij Emelyanov on 26/01/2019.
//  Copyright © 2019 Georgij Emelyanov. All rights reserved.
//

import Foundation

extension NumberFormatter {
    static func numberFormatter() -> NumberFormatter {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .none
        numberFormatter.maximumFractionDigits = 2
        numberFormatter.minimumFractionDigits = 0
        numberFormatter.generatesDecimalNumbers = true
        numberFormatter.roundingMode = .down
        numberFormatter.minimumIntegerDigits = 1
        return numberFormatter
    }
    
    func stringFrom(_ value: Decimal) -> String? {
        return self.string(from: NSNumber(value: value.doubleValue))
    }
    
    func stringFrom(_ value: Double) -> String? {
        return self.string(from: NSNumber(value: value))
    }
}
