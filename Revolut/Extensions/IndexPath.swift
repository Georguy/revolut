//
//  IndexPath.swift
//  Revolut
//
//  Created by Georgij Emelyanov on 07/02/2019.
//  Copyright © 2019 Georgij Emelyanov. All rights reserved.
//

import Foundation

extension IndexPath {
    static let firstRow = IndexPath(row: 0, section: 0)
}
