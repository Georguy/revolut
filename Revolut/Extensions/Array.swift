//
//  Array.swift
//  Revolut
//
//  Created by Georgij Emelyanov on 26/01/2019.
//  Copyright © 2019 Georgij Emelyanov. All rights reserved.
//

import Foundation

extension Array {
    public subscript(safe index: Int) -> Element? {
        return indices ~= index ? self[index] : nil
    }
    
    mutating func move(from oldIndex: Index, to newIndex: Index) {
        guard indices ~= oldIndex, indices ~= newIndex, oldIndex != newIndex else { return }
        guard abs(newIndex - oldIndex) != 1 else { return swapAt(oldIndex, newIndex) }
        insert(remove(at: oldIndex), at: newIndex)
    }
}

extension Array where Element: Equatable {
    mutating func move(_ element: Element, to newIndex: Index) {
        guard indices ~= newIndex, let oldIndex: Int = self.index(of: element) else { return }
        move(from: oldIndex, to: newIndex)
    }
    
    mutating func replace(_ object: Element) {
        guard let index = self.firstIndex(of: object) else { return }
        remove(at: index)
        insert(object, at: index)
    }
    
    mutating func remove(_ object: Element) {
        guard let index = self.firstIndex(of: object) else { return }
        remove(at: index)
    }
}
