//
//  JSONReader.swift
//  RevolutTests
//
//  Created by Georgij Emelyanov on 04/02/2019.
//  Copyright © 2019 Georgij Emelyanov. All rights reserved.
//

import Foundation

class JSONReader {
    private init() { }
    static func getJSON(name: String) -> Data {
        do {
            return try Data(contentsOf: URL(fileURLWithPath: path(name: name, type: "json")))
        } catch {
            fatalError("Couldn't read \(name)")
        }
    }
    
    static func path(name: String, type: String) -> String {
        guard let path = Bundle(for: self).path(forResource: name, ofType: type) else {
            fatalError("Error while reading \(name)")
        }
        return path
    }
}
