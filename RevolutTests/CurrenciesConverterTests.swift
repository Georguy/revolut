//
//  CurrenciesConverterTests.swift
//  RevolutTests
//
//  Created by Georgij Emelyanov on 07/02/2019.
//  Copyright © 2019 Georgij Emelyanov. All rights reserved.
//

import XCTest
@testable import Revolut

class CurrenciesConverterTests: XCTestCase {

    let converter = CurrenciesConverter()
    
    func testNotEmptyResult() {
        let expectation = XCTestExpectation(description: "wait for handling process")
        
        converter.convert(TestData.currencies, value: 1) { viewModels in
            XCTAssertTrue(!viewModels.isEmpty, "View Models are empty")
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 100)
    }
    
    func testRightValueConvertToDecimal() {
        let result = converter.convertStringToDouble("1.5678")
        
        XCTAssertEqual(result, 1.5678, "Wrong answer")
    }
}

extension CurrenciesConverterTests {
    enum TestData {
        static let currencies: [Currency] = [Currency(iso: "EUR", rate: 1),
                                             Currency(iso: "AUD", rate: 1.6236),
                                             Currency(iso: "BGN", rate: 1.9645),
                                             Currency(iso: "BRL", rate: 4.8131),
                                             ]
    }
}
