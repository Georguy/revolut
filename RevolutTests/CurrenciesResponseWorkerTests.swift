//
//  CurrenciesResponseWorkerTests.swift
//  RevolutTests
//
//  Created by Georgij Emelyanov on 04/02/2019.
//  Copyright © 2019 Georgij Emelyanov. All rights reserved.
//

import XCTest
@testable import Revolut

class CurrenciesResponseWorkerTests: XCTestCase {
    
    private let worker = CurrenciesResponseWorker()
    
    private var response: CurrenciesResponse!
    
    override func setUp() {
        response = try! CurrenciesResponse.from(data: JSONReader.getJSON(name: "Response"))
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testHandledResultIsNotEmpty() {
        let expectation = XCTestExpectation(description: "wait for handling process")
        
        worker.handleResponse(response, oldCurrencies: []) { currencies in
            XCTAssertTrue(!currencies.isEmpty, "Currencies are empty")
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 100)
    }
    
    func testNewCurrenciesHaveTheSameOrder() {
        let currenciesStub: [Currency] = TestData.originCurrencies
        
        let expectation = XCTestExpectation(description: "wait for handling process")
        
        worker.handleResponse(response, oldCurrencies: currenciesStub) { currencies in
            XCTAssertEqual(currenciesStub, currencies, "New currencies are something else")
            
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 100)
    }
    
    func testNewCurrenciesHaveNewOrder() {
        let currenciesStub: [Currency] = TestData.shuffledCurrencies
        
        let expectation = XCTestExpectation(description: "wait for handling process")
        
        worker.handleResponse(response, oldCurrencies: currenciesStub) { currencies in
            XCTAssertNotEqual(currenciesStub, currencies, "New currencies are something else")
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 100)
    }
}

extension CurrenciesResponseWorkerTests {
    enum TestData {
        static let originCurrencies: [Currency] = [Currency(iso: "EUR", rate: 1),
                                                   Currency(iso: "AUD", rate: 1.6236),
                                                   Currency(iso: "BGN", rate: 1.9645),
                                                   Currency(iso: "BRL", rate: 4.8131),
                                                   ]
        
        static let shuffledCurrencies: [Currency] = [Currency(iso: "BGN", rate: 1),
                                                     Currency(iso: "EUR", rate: 1.321),
                                                     Currency(iso: "AUD", rate: 1.6236),
                                                     Currency(iso: "BRL", rate: 4.8131),
                                                     ]
    }
}

