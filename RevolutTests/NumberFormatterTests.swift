//
//  NumberFormatterTests.swift
//  RevolutTests
//
//  Created by Georgij Emelyanov on 07/02/2019.
//  Copyright © 2019 Georgij Emelyanov. All rights reserved.
//

import XCTest
@testable import Revolut

class NumberFormatterTests: XCTestCase {
    let formatter = NumberFormatter.numberFormatter()
    
    func testStringIsNotNil() {
        let data = TestData.rate * 1.0
        let result = formatter.stringFrom(data)
        XCTAssertNotNil(result, "Result is ni")
    }
    
    func testStringIsCorrect() {
        let data = TestData.rate * 1.0
        let result = formatter.stringFrom(data)!
        XCTAssertEqual(result, "1.59", "Result isn't correct")
    }
    
    func testStringIsCorrect2() {
        let data = TestData.rate * 5.5
        let result = formatter.stringFrom(data)!
        XCTAssertEqual(result, "8.79", "Result isn't correct")
    }
}

extension NumberFormatterTests {
    enum TestData {
        static let rate = 1.5986
    }
}
