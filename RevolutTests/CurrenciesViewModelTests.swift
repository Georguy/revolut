//
//  CurrenciesViewModelTests.swift
//  RevolutTests
//
//  Created by Georgij Emelyanov on 07/02/2019.
//  Copyright © 2019 Georgij Emelyanov. All rights reserved.
//

import XCTest
@testable import Revolut

class CurrenciesViewModelTests: XCTestCase {
    
    let currenciesServiceMock = CurrenciesServiceMock()
    let currenciesWorkerMock = CurrenciesResponseWorkerMock()
    let currenciesConverter = CurrenciesConverterMock()
    
    var viewModel: CurrenciesViewModel!
    
    override func setUp() {
        viewModel = CurrenciesViewModel(currenciesService: currenciesServiceMock, currenciesWorker: currenciesWorkerMock, currenciesConverter: currenciesConverter)
    }
    
    func testCurrenciesServiceWasCalled() {
        currenciesServiceMock.fetchCurrenciesCompletionStub = .success(TestData.baseResponse)
        viewModel.getCurrencies(by: TestData.base)
        XCTAssertEqual(currenciesServiceMock.fetchCurrenciesWasCalled, 1, "currenciesServiceMock wasn't called")
    }
    
    func testCurrenciesResponseWorkerWasNotCalled() {
        currenciesServiceMock.fetchCurrenciesCompletionStub = .success(TestData.responseWithNewBase)
        viewModel.getCurrencies(by: TestData.base)
        XCTAssertEqual(currenciesWorkerMock.handleResponseWasCalled, 0, "currenciesWorkerMock wasn't called")
    }
    
    func testCurrenciesResponseWorkerWasCalled() {
        currenciesServiceMock.fetchCurrenciesCompletionStub = .success(TestData.baseResponse)
        viewModel.getCurrencies(by: TestData.base)
        XCTAssertEqual(currenciesWorkerMock.handleResponseWasCalled, 1, "currenciesWorkerMock wasn't called")
    }
    
    func testCurrenciesConverterWasCalled() {
        currenciesServiceMock.fetchCurrenciesCompletionStub = .success(TestData.baseResponse)
        viewModel.getCurrencies(by: TestData.base)
        XCTAssertEqual(currenciesConverter.convertWasCalled, 1, "currenciesConverterMock wasn't called")
    }
    
    func testUpdateValueStringDidCallConverter() {
        viewModel.updateValueString("1.5678")
        XCTAssertEqual(currenciesConverter.convertStringToDoubleWasCalled, 1, "currenciesConverterMock wasn't called")
        XCTAssertEqual(currenciesConverter.convertWasCalled, 1, "currenciesConverterMock wasn't called")
    }
    
    func testUpdateBaseCurrencyDidCallConverter() {
        currenciesServiceMock.fetchCurrenciesCompletionStub = .success(TestData.baseResponse)
        viewModel.updateBaseCurrency(TestData.currenciesViewModels[0], atIndex: 1)
        XCTAssertEqual(currenciesConverter.convertStringToDoubleWasCalled, 1, "currenciesConverterMock wasn't called")
    }
    
    func testHandleOnErrorCase() {
        let expectation = XCTestExpectation(description: "wait for onErro closure")
        currenciesServiceMock.fetchCurrenciesCompletionStub = .failure(ErrorMock())
        viewModel.onError = {
            XCTAssert(true, "onError wasn't called")
            expectation.fulfill()
        }
        viewModel.getCurrencies(by: TestData.base)
        wait(for: [expectation], timeout: 100)
    }
}

class ErrorMock: Error {
    
}

extension CurrenciesViewModelTests {
    enum TestData {
        static let base: ISO = "EUR"
        static let baseResponse: CurrenciesResponse = .init()
        static let responseWithNewBase: CurrenciesResponse = .init(base: "USD")
        
        static let currenciesViewModels: [CurrencyViewModel] = [CurrencyViewModel(iso: "BGN", sumString: "1"),
                                                                CurrencyViewModel(iso: "EUR", sumString: "1.32"),
                                                                CurrencyViewModel(iso: "AUD", sumString: "1.62"),
                                                                CurrencyViewModel(iso: "BRL", sumString: "4.81"),
                                                                ]
    }
}

class CurrenciesServiceMock: CurrenciesServiceProtocol {
    public var fetchCurrenciesWasCalled: Int = 0
    public var fetchCurrenciesCompletionStub: Result<CurrenciesResponse>!
    
    func fetchCurrencies(by base: ISO, completion: @escaping ((Result<CurrenciesResponse>) -> Void)) {
        fetchCurrenciesWasCalled += 1
        completion(fetchCurrenciesCompletionStub)
    }
}

class CurrenciesResponseWorkerMock: CurrenciesResponseWorkerProtocol {
    
    public var handleResponseWasCalled: Int = 0
    public var handleResponseCompletionStub: [Currency] = .init()
    
    func handleResponse(_ response: CurrenciesResponse, oldCurrencies: [Currency], completion: @escaping ([Currency]) -> Void) {
        handleResponseWasCalled += 1
        completion(handleResponseCompletionStub)
    }
}

class CurrenciesConverterMock: CurrenciesConverterProtocol {
    
    public var convertWasCalled: Int = 0
    public var convertStringToDoubleWasCalled: Int = 0
    
    func convert(_ currencies: [Currency], value: Double, completion: @escaping ([CurrencyViewModel]) -> Void) {
        convertWasCalled += 1
        
    }
    
    func convertStringToDouble(_ sumString: String) -> Double? {
        convertStringToDoubleWasCalled += 1
        return nil
    }
}
